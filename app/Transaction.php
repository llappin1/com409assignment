<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'TranDate','Total','CreditcardNum','CCardType','CCardExpiry'
    ];
}
