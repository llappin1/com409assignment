<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;

use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Return books page
     *
     * @param Request $request
     * @return
     */
    public function books(Request $request)
    {
        $level = null;
        if (!empty($request->get('q'))) {
            //Get books by specific search query
            $books = Book::query()
                ->where('title', 'LIKE', '%' . $request->get('q') . '%')
                ->get();
        } elseif (!empty($request->get('level'))) {
            //Get books by specific level
            $books = Book::query()
                ->where('level_id', $request->get('level'))
                ->get();

            $level = \App\Level::find($request->get('level'));
        } else {
            //Get all books
            $books = Book::all();
        }

        $levels = \App\Level::all()->keyBy('id');

        return view('books', [
            'books'       => $books,
            'level'       => $level,
            'levels'      => $levels,
            'searchQuery' => $request->get('q')
        ]);
    }

    /**
     * Show add book page
     *
     * @return mixed
     */
    public function addBook()
    {
        //If user is nto admin redirect to books page
        if (auth()->user()->type == 1) {
            return redirect('/books');
        }

        $levels = \App\Level::all();
        $authors = \App\Author::all();

        return view('add-book', [
            'levels'  => $levels,
            'authors' => $authors,
        ]);
    }

    /**
     * Add book
     *
     * @return mixed
     */
    public function addBookPost(Request $request)
    {
        //If user is nto admin redirect to books page
        if (auth()->user()->type == 1) {
            return redirect('/books');
        }

        //Validate data
        $request->validate([
            'title'     => 'required|string',
            'price'     => 'required|numeric|min:1',
            'language'  => 'required|string',
            'pub_date'  => 'required|string',
            'level_id'  => 'required|integer',
            'author_id' => 'required|integer',
            'image'     => 'required',
        ]);

        $file = $request->image;

        $directory = public_path('uploads/books');
        if (!file_exists($directory)) {
            mkdir($directory, 0755, true);
        }

        $filename = "book_" . sha1(time() . time()) . "." . $file->extension();
        $file->move($directory, $filename);

        //Create book
        Book::create([
            'title'      => $request->get('title'),
            'price'      => $request->get('price'),
            'language'   => $request->get('language'),
            'pub_date'   => Carbon::createFromFormat('d/m/Y', $request->get('pub_date')),
            'level_id'   => $request->get('level_id'),
            'author_id'  => $request->get('author_id'),
            'image_path' => $filename,
        ]);

        //Redirect to books page
        return redirect()->route('books');
    }

    /**
     * Delete book
     *
     * @return mixed
     */
    public function deleteBook(Request $request)
    {
        //If user is nto admin redirect to books page
        if (auth()->user()->type == 1) {
            return redirect('/books');
        }

        $book = Book::find($request->route('id'));

        if(!empty($book)) {
            //Delete book
            $book->delete();
        }

        //Redirect to books page
        return redirect()->route('books');
    }
}
