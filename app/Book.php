<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title',
        'price',
        'language',
        'author_id',
        'level_id',
        'pub_date',
        'image_path',
    ];

    /**
     * Returns author relationship
     *
     * @return mixed
     */
    public function author() {
        return $this->hasOne(
            Author::class,
            'id',
            'author_id'
        );
    }

    /**
     * Returns level relationship
     *
     * @return mixed
     */
    public function level() {
        return $this->hasOne(
            Level::class,
            'id',
            'level_id'
        );
    }
}
