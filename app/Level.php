<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title',
    ];

    /**
     * Returns books relationship
     *
     * @return mixed
     */
    public function books() {
        return $this->hasMany(
            Book::class,
            'level_id',
            'id'
        );
    }
}
