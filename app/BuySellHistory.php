<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuySellHistory extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'SellerID','ISBN','TransactID'
    ];
}
