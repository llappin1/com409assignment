<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserTableSeeder::class,
            AuthorTableSeeder::class,
            LevelTableSeeder::class,
            BooksTableSeeder::class,
//            RecommendedTableSeeder::class,
//            SellerTableSeeder::class,
//            WrittenTableSeeder::class,
//            TransactionTableSeeder::class,
//            WrittenTableSeeder::class,
//            BuySellHistoryTableSeeder::class,
//            MembersTableSeeder::class,
//            CourseTableSeeder::class,


       ]);
    }
}
