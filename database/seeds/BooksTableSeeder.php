<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Book::create([
            'title'      => 'Networks BOOK',
            'price'      => 38,
            'language'   => 'English',
            'author_id'  => 1,
            'level_id'   => 1,
            'pub_date'   => '2008-07-01',
            'image_path' => 'introCompNet.jpg'
        ]);

        \App\Book::create([
            'title'      => 'SQL BOOK',
            'price'      => 30,
            'language'   => 'English',
            'author_id'  => 1,
            'level_id'   => 1,
            'pub_date'   => '2010-09-12',
            'image_path' => 'LearningSQL.jpg'
        ]);
        \App\Book::create([
            'title'      => 'Guide to SQL BOOK',
            'price'      => 38,
            'language'   => 'English',
            'author_id'  => 1,
            'level_id'   => 1,
            'pub_date'   => '2003-04-21',
            'image_path' => 'guideSQL.jpg'
        ]);
        \App\Book::create([
            'title'      => 'Java BOOK',
            'price'      => 40,
            'language'   => 'English',
            'author_id'  => 1,
            'level_id'   => 2,
            'pub_date'   => '2014-07-02',
            'image_path' => 'Java.jpg'
        ]);

        \App\Book::create([
            'title'      => 'Python BOOK',
            'price'      => 25,
            'language'   => 'English',
            'author_id'  => 1,
            'level_id'   => 2,
            'pub_date'   => '2018-03-10',
            'image_path' => 'cambridgeCsPython.jpg'
        ]);
        \App\Book::create([
            'title'      => 'Visual Basic BOOK',
            'price'      => 25,
            'language'   => 'English',
            'author_id'  => 1,
            'level_id'   => 2,
            'pub_date'   => '2009-10-05',
            'image_path' => 'cambridgeCsVisualBasic.jpg'
            ]);

            \App\Book::create([
                'title'      => 'ProGit BOOK',
                'price'      => 45,
                'language'   => 'English',
                'author_id'  => 1,
                'level_id'   => 2,
                'pub_date'   => '2005-05-14',
                'image_path' => 'ProGit.jpg'
            ]);
        \App\Book::create([
            'title'      => 'PHP BOOK',
            'price'      => 38,
            'language'   => 'English',
            'author_id'  => 1,
            'level_id'   => 3,
            'pub_date'   => '2012-01-17',
            'image_path' => 'PHPBook.jpg'
        ]);


    }
}
