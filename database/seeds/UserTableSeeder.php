<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Normal user
        User::create([
            'name'     => 'Test User',
            'email'    => 'test@email.com',
            'password' => Hash::make('user123'),
            'type'     => 1
        ]);

        //Admin user
        User::create([
            'name'     => 'Admin User',
            'email'    => 'admin@email.com',
            'password' => Hash::make('admin123'),
            'type'     => 2
        ]);
    }
}
