<?php

use Illuminate\Database\Seeder;

class BuySellHistoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\BuySellHistory::class, 50)->create();
    }
}
