<?php

use Faker\Generator as Faker;

$factory->define(App\Transaction::class, function (Faker $faker) {
    return [
        'TranDate' => $faker-> date($format = 'Y-m-d', $max = 'now') ,
        'Total' => $faker-> randomNumber(2),
        'CreditcardNum' => $faker->creditCardNumber,
        'CCardType' => $faker-> creditCardType,
        'CCardExpiry'=> $faker-> creditCardExpirationDate,
    ];
});
