<?php

use App\Book;
use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker) {
    return [
             'ISBN' => $faker-> isbn13 ,
            'Title' => $faker-> sentence(6,true),
            'Price' => $faker->randomNumber(2),
            'Language' => $faker-> country,
            'Publisher'=> $faker-> company,
            'pub_date'=> $faker-> date($format = 'Y-m-d', $max = 'now'),
            'MemberID'=> App\Member::inRandomOrder()->first()->MemberID
        //
    ];
});
