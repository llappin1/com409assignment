<?php

use Faker\Generator as Faker;

$factory->define(App\Seller::class, function (Faker $faker) {
    return [
        'NumOfCopies'=> $faker-> randomDigitNotNull,
        'Price'=> $faker->randomNumber(2),
        'ISBN' => App\Book::inRandomOrder()->first()->ISBN

    ];
});
