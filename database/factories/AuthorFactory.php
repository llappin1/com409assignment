<?php

use Faker\Generator as Faker;

$factory->define(App\Author::class, function (Faker $faker) {
    return [
        'Forename' => $faker-> firstName ,
        'Surname' => $faker-> lastName,
        'Email' => $faker-> unique()->safeEmail

        //
    ];
});
