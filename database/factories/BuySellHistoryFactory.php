<?php

use Faker\Generator as Faker;

$factory->define(App\BuySellHistory::class, function (Faker $faker) {
    return [
        'SellerID'=> App\Seller::inRandomOrder()->first()->SellerID,
        'ISBN'=> App\Book::inRandomOrder()->first()->ISBN,
        'TransactID'=> App\Transaction::inRandomOrder()->first()->TransactID
    ];
});
