<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
//use Illuminate\Database\QueryException;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('books');
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->decimal('price', 4, 2);
            $table->string('language');

            $table->unsignedInteger('author_id');
            $table->foreign('author_id')
                  ->references('id')->on('authors')
                  ->onDelete('cascade');

            $table->unsignedInteger('level_id');
            $table->foreign('level_id')
                ->references('id')->on('levels')
                ->onDelete('cascade');

            $table->date('pub_date');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
