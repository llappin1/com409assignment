<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuySellHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('buy_sell_histories');
        Schema::create('buy_sell_histories', function (Blueprint $table) {
            $table->Increments('BuyerID');
            $table->unsignedInteger('SellerID');
            $table->string('ISBN');
            $table->unsignedInteger('TransactID');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buy_sell_histories');
    }
}
