@extends('layouts.main')

@section('content')
	<section id="showcase">
		<div class="container">
			<h1>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.
			</h1>
		</div>
	</section>

	<div class="container">
		<section id="main">
			<h1>About Us</h1>
			<p>Computing Central will be an ecommerce site especially for computing students to buy and sell relevant learning materials such as books on computing related topics.
				The main concept of the site is too allow students to easily text related to their course and learning development.
				Books will be searchable by course or modules that they are related too to ensure that finding the correct material is easy for users.
				The site will allow users to view the collection of texts for sale or search for a specific book. Users will have to log on in order to buy or sell books to ensure that personal details are kept secure.
				Registered users with an account will be able to access pages of extra information and tips on topics such as programming languages, hardware and software development tools.
				This is too encourage more students to make use of the site.</p>

		</section>


	</div>
@endsection