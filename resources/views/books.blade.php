@extends('layouts.main')

@section('content')

	<div class="row p-1">
		<div class="col-8">
			@if(!empty($level))
				<h2>Book Level: {{ $level->title }}</h2>
			@endif
		</div>
		<div class="col-4 float-right text-right">
			@if(!empty(auth()->user()) && auth()->user()->type == 2)
				<a href="{{ route('add-book') }}" class="btn btn-sm btn-success">Add Book</a>
			@endif
		</div>
	@forelse($books as $book)
			<div class="col-3 card">
				<img src="{{ asset('uploads/books/' . $book->image_path) }}" class="card-img-top" alt="Book Image">
				<div class="card-body">
					<h5 class="card-title">{{ $book->title }} - £{{ number_format($book->price, 2) }}</h5>
					<p class="card-text">
						<div class="row">
							<div class="col-4">
								<strong>Author</strong>
							</div>
							<div class="col-8">
								{{ $book->author->forename }} {{ $book->author->surname }} (<a href="mailto:{{ $book->author->email }}">{{ $book->author->email }}</a>)
							</div>
						</div>
						<div class="row">
							<div class="col-4">
								<strong>Publish Date</strong>
							</div>
							<div class="col-8">
								{{ Carbon\Carbon::parse($book->pub_date)->format('d/m/Y') }}
							</div>
						</div>
						<div class="row">
							<div class="col-4">
								<strong>Language</strong>
							</div>
							<div class="col-8">
								{{ $book->language }}
							</div>
						</div>
						<div class="row">
							<div class="col-4">
								<strong>Level</strong>
							</div>
							<div class="col-8">
								{{ $book->level->title }} <a href="/books?level={{ $book->level->getKey() }}">({{count($levels[$book->level->getKey()]->books)}} {{ str_plural('Book', count($levels[$book->level->getKey()]->books)) }})</a>
							</div>
						</div>
					</p>
					@if(!empty(auth()->user()))
						<script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
								data-key="pk_test_TYooMQauvdEDq54NiTphI7jx"
								data-amount="{{ $book->price * 100 }}"
								data-zip-code="true"></script>
						@if(auth()->user()->type == 2)
							<a href="{{ route('delete-book', [$book->getKey()]) }}" class="btn btn-sm btn-danger ml-2">Delete</a>
						@endif
					@else
						<a class="btn btn-primary btn-sm" href="{{ route('login') }}">{{ __('Login') }}</a>
						<a class="btn btn-secondary ml-2 btn-sm" href="{{ route('register') }}">{{ __('Register') }}</a>
					@endif

				</div>
			</div>
	@empty
		<div class="col-12">
			<p>There is no books by this search query.</p>
		</div>
	@endforelse
@endsection