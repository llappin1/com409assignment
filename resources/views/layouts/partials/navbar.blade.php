<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/">Home</a>
            </li>
            <li class="nav-item">
               <a class="nav-link" href="/books">Books</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/about">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/contact">Contact Us</a>
            </li>
            @guest
                <li class="nav-item">
                    <a class="nav-link btn btn-primary btn-sm" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link btn btn-secondary ml-2 btn-sm" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
                @endif

            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                    </form>
                    </div>
                </li>
            @endguest
        </ul>
        <form class="form-inline my-2 my-lg-0" action="{{ route('books') }}">
            <input class="form-control mr-sm-2" type="search" name="q" placeholder="Search" aria-label="Search" value="{{ isset($searchQuery) ? $searchQuery : '' }}">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>

    </div>
</nav>
{{--<nav id="navbar">--}}
    {{--<div class="container">--}}
        {{--<ul>--}}
            {{--<li><a href="/">Home</a></li>--}}
            {{--<li><a href="books">id="navbar"</a></li>--}}
            {{--<li><a href="about">About</a></li>--}}
            {{--<li><a href="contact">Contact Us</a></li>--}}
            {{--<li>--}}
                {{--<form class="form-inline" method="GET" action="{{ route('books') }}" role="search">--}}
                    {{--<input class="form-control" name="q" type="search" placeholder="Search" aria-label="Search" value="{{ isset($searchQuery) ? $searchQuery : '' }}">--}}
                    {{--<span class="input-group-btn">--}}
							{{--<button class="btn btn-outline-success my-2 my-sm-0" type="submit">--}}
								{{--<span class="glyphicon glyphicon-search">--}}
								{{--</span>--}}
								{{--Search--}}
							{{--</button>--}}
						{{--</span>--}}
                {{--</form>--}}
                {{--@if(isset($details))--}}
                    {{--<p> The Search results for your query <b> {{ $query }} </b> are :</p>--}}
                    {{--<h2>Sample User details</h2>--}}
                    {{--<table class="table table-striped">--}}
                        {{--<thead>--}}
                        {{--<tr>--}}
                            {{--<th>Book</th>--}}
                            {{--<th>Price</th>--}}
                        {{--</tr>--}}
                        {{--</thead>--}}
                        {{--<tbody>--}}
                        {{--@foreach($details as $user)--}}
                            {{--<tr>--}}
                                {{--<td>{{$user->name}}</td>--}}
                                {{--<td>{{$user->email}}</td>--}}
                            {{--</tr>--}}
                        {{--@endforeach--}}
                        {{--</tbody>--}}
                    {{--</table>--}}
                {{--@endif--}}
            {{--</li>--}}

            {{--@guest--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>--}}
                {{--</li>--}}
                {{--@if (Route::has('register'))--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
                    {{--</li>--}}
                {{--@endif--}}
            {{--@else--}}
                {{--<li class="nav-item dropdown">--}}
                    {{--<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>--}}
                        {{--{{ Auth::user()->name }} <span class="caret"></span>--}}
                    {{--</a>--}}

                    {{--<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">--}}
                        {{--<a class="dropdown-item" href="{{ route('logout') }}"--}}
                           {{--onclick="event.preventDefault();--}}
                                                     {{--document.getElementById('logout-form').submit();">--}}
                            {{--{{ __('Logout') }}--}}
                        {{--</a>--}}

                        {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                            {{--@csrf--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</li>--}}
            {{--@endguest--}}
        {{--</ul>--}}

    {{--</div>--}}
{{--</nav>--}}