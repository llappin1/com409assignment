<!DOCTYPE html>
<html>
<head>
	<title>Computing Central - Search</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<header id="main-header">
		<div class="container">
			<h1>Computing Central</h1>
		</div>
	</header>

	<nav id="navbar">
		<div class="container">
			<ul>
				<li><a href="/">Home</a></li>
				<li><a href="books">Books</a></li>
				<li><a href="about">About</a></li>
				<li><a href="contact">Contact Us</a></li>
				<li><input type="text" name="searchbar" placeholder="Search..."></li>
				<form class="form-inline">
					<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				</form>
				@guest
					<li class="nav-item">
						<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
					</li>
					@if (Route::has('register'))
						<li class="nav-item">
							<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
						</li>
					@endif
				@else
					<li class="nav-item dropdown">
						<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
							{{ Auth::user()->name }} <span class="caret"></span>
						</a>

						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="{{ route('logout') }}"
							   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
								{{ __('Logout') }}
							</a>

							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								@csrf
							</form>
						</div>
					</li>
				@endguest
			</ul>
			<ul class="navbar-nav ml-auto">
				<!-- Authentication Links -->
				@guest
					<li class="nav-item">
						<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
					</li>
					@if (Route::has('register'))
						<li class="nav-item">
							<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
						</li>
					@endif
				@else
					<li class="nav-item dropdown">
						<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
							{{ Auth::user()->name }} <span class="caret"></span>
						</a>

						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="{{ route('logout') }}"
							   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
								{{ __('Logout') }}
							</a>

							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								@csrf
							</form>
						</div>
					</li>
				@endguest
			</ul>
		</div>
	</nav>

	<section id="showcase">
		<div class="container">
			<h1>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.
			</h1>
		</div>	
	</section>

	<div class="container">
		<section id="main">
			<h1>Search</h1>
			<div class="search-container">
				Lorem
			</div>
			<div>

				<div class="container">
					@if(isset($details))
						<p> The Search results for your query <b> {{ $query }} </b> are :</p>
						<h2>Sample User details</h2>
						<table class="table table-striped">
							<thead>
							<tr>
								<th>Name</th>
								<th>Email</th>
							</tr>
							</thead>
							<tbody>
							@foreach($details as $user)
								<tr>
									<td>{{$user->name}}</td>
									<td>{{$user->email}}</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					@endif
				</div>
			</div>
		</section>


	</div>

	<footer id="main-footer">
		<p>Copyright &copy; 2018 Computing Central</p>
	</footer>

</body>
</html>