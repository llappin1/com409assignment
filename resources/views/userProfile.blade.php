<!DOCTYPE html>
<html>
<head>
	<title>Computing Central - User Profile</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<header id="main-header">
		<div class="container">
			<h1>Computing Central - User Profile</h1>
		</div>
	</header>

	<nav id="navbar">
		<div class="container">
			<ul>
				<li><a href="/">Home</a></li>
				<li><a href="books">Books</a></li>
				<li><a href="about">About</a></li>
				<li><a href="contact">Contact Us</a></li>
				<li><input type="text" name="searchbar" placeholder="Search..."></li>
			</ul>
		</div>
	</nav>

	<section id="showcase">
		<div class="container">
			<h1>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.
			</h1>
		</div>	
	</section>

<div class="container">
		<section id="main">
			<h1>User</h1>
		<section id="main">
			<h1>Books</h1>
			<p>Bought</p>
		<div class="block">
			<h3>Intro to Networks</h3>
			<hr>
			<img src="img/introCompNet.jpg">
			<hr>			
			<p>Lorem ipsum dolor, sit amet,  $##.##</p>
		</div>
		<div class="block">
			<h3>Learning SQL</h3>
			<hr>
			<img src="img/learningSQL.jpg">
			<hr>
			<p>Lorem ipsum dolor, sit amet,  $##.##</p>
		</div>
		<div class="block">
			<h3>CS Python</h3>
			<hr>
			<img src="img/cambridgeCsPyhton.jpg">
			<hr>	
			<p>Lorem ipsum dolor, sit amet,  $##.##</p>
		</div>
	</section>

<section id="main">
	<h1>Sold</h1>
		<div class="block">
			<h3>Intro to Networks</h3>
			<hr>
			<img src="img/introCompNet.jpg">
			<hr>			
			<p>Lorem ipsum dolor, sit amet,  $##.##</p>
		</div>
		<div class="block">
			<h3>Learning SQL</h3>
			<hr>
			<img src="img/learningSQL.jpg">
			<hr>
			<p>Lorem ipsum dolor, sit amet,  $##.##</p>
		</div>
		<div class="block">
			<h3>CS Python</h3>
			<hr>
			<img src="img/cambridgeCsPyhton.jpg">
			<hr>	
			<p>Lorem ipsum dolor, sit amet,  $##.##</p>
		</div>


</section>
		</section>


	</div>

	<footer id="main-footer">
		<p>Copyright &copy; 2018 Computing Central</p>
	</footer>

</body>
</html>