<!DOCTYPE html>
<html>
<head>
	<title>Computing Central - Profile</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<header id="main-header">
		<div class="container">
			<h1>Computing Central</h1>
		</div>
	</header>

	<nav id="navbar">
		<div class="container">
			<ul>
				<li><a href="/">Home</a></li>
				<li><a href="books">Books</a></li>
				<li><a href="about">About</a></li>
				<li><a href="contact">Contact Us</a></li>
				<li><input type="text" name="searchbar" placeholder="Search..."></li>
			</ul>
		</div>
	</nav>

	<section id="showcase">
		<div class="container">
			<h1>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.
			</h1>
		</div>	
	</section>

	<div class="container">
		<section id="main">
			<form class="my-form">
				<div class="form-group">
					<h1>Profile</h1>
					<label>Username</label>
				<input type="text" name="userName" placeholder="Enter Username">
	</div>
	<div class="form-group">
		<label>Password</label>
		<input type="Password" name="password" placeholder="Enter Password">
	</div>
			<button  onclick= "location.href = 'newUser.html';" type="button">Create Account</button>
			<button type="button">Login</button>
</form>
		</section>


	</div>

	<footer id="main-footer">
		<p>Copyright &copy; 2018 Computing Central</p>
	</footer>

</body>
</html>