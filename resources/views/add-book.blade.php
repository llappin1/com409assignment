@extends('layouts.main')

@section('content')

	<div class="row p-1 m-2 mb-5">
		<div class="col-12">
			<h2>Add Book</h2>
		</div>
		<form action="" class="col-4" method="POST" enctype='multipart/form-data'>

			{{ csrf_field() }}

			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif

			<div class="form-group">
				<label for="title">Title</label>
				<input type="text" name="title" id="title" class="form-control">
			</div>

			<div class="form-group">
				<label for="price">Price</label>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text" id="basic-addon1">£</span>
					</div>
					<input type="number" name="price" id="price" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="language">Language</label>
				<input type="text" name="language" id="language" class="form-control">
			</div>

			<div class="form-group">
				<label for="pub_date">Publish Date</label>
				<input type="text" name="pub_date" id="pub_date" class="form-control datepicker">
			</div>

			<div class="form-group">
				<label for="level_id">Level</label>
				<select name="level_id" id="level_id" class="form-control">
					@foreach($levels as $level)
						<option value="{{ $level->getKey() }}">{{ $level->title }}</option>
					@endforeach
				</select>
			</div>

			<div class="form-group">
				<label for="author_id">Author</label>
				<select name="author_id" id="author_id" class="form-control">
					@foreach($authors as $author)
						<option value="{{ $author->getKey() }}">{{ $author->forename }} {{ $author->surname }}</option>
					@endforeach
				</select>
			</div>

			<div class="form-group">
				<label for="image">Image</label>
				<input type="file" name="image" id="image" class="form-control">
			</div>

			<div class="form-group">
				<input type="submit" class="btn btn-primary" value="Submit">
			</div>
		</form>
	</div>
	<script>
		$(function() {
            $("input[name='pub_date']").datepicker();
		});
	</script>
@endsection