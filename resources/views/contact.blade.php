@extends('layouts.main')

@section('content')
	<section id="showcase">
		<div class="container">
			<h1>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.
			</h1>
		</div>
	</section>

	<div class="container">
		<section id="main" class="row">
			<div class="col-12">
				<h1>Contact Us</h1>
			</div>
			<form class="contact-form col-4" method="post" action="mailto:campbell-s79@ulster.ac.uk">
				<div class="form-group">
					<label>Name: </label>
					<input type="text" name="userName" placeholder="Enter Name" class="form-control">
				</div>
				<div class="form-group">
					<label>E-mail: </label>
					<input type="email" name="email" placeholder="Enter email" class="form-control">
				</div>
				<div class="form-group">
					<label>Message: </label>
					<textarea name="message" placeholder="Enter message" class="form-control"></textarea>
				</div>
				<input type="submit" name="submit" id="submit" class="btn btn-primary" value="Submit">
				<input type="reset" name="reset" id="button"  class="btn btn-default" value="Reset">
			</form>
			<div class="col-6 offset-2">
				<div style="width: 100%"><iframe width="100%" height="400" src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;coord=54.68936055,-5.887104422912074&amp;q=Shore%20Rd%2C%20Newtownabbey%20BT37%200QB+(Ulster%20University)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/map-my-route/">Draw map route</a></iframe></div><br />
			</div>
		</section>

	</div>
@endsection