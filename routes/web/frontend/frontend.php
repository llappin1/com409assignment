<?php
/**
 * Created by PhpStorm.
 * User: b00715734
 * Date: 22/03/2019
 * Time: 13:53
 */

Route::get('/', function () {
    return view('index');
});
Route::get('home', function () {
    return view('index');
});
Route::get('admin', function () {
    return view('index');
});


Route::post('/search', [
    'as'   => 'search',
    'uses' => 'HomeController@search'
]);
//
//Route::post('/search', function () {
//    $q = Input::get('q');
//    if ($q != "") {
//        $user = User::where('title', 'LIKE', '%' . $q . '%')
//            ->orWhere('books', 'LIKE', '%' . $q . '%')
//            ->get();
//        if (count($user) > 0)
//            return view('index')->withDetails($user)->withQuery($q);
//        else
//            return view('search-results')->withMessage("No books found");
//    }
//});

Route::get('/books', [
    'as'   => 'books',
    'uses' => 'HomeController@books'
]);

Route::get('/add-book', [
    'as'   => 'add-book',
    'uses' => 'HomeController@addBook'
])->middleware('auth');

Route::get('/delete-book/{id}', [
    'as'   => 'delete-book',
    'uses' => 'HomeController@deleteBook'
])->middleware('auth');

Route::post('/add-book', [
    'as'   => 'add-book',
    'uses' => 'HomeController@addBookPost'
])->middleware('auth');

Route::get('newUser', function () {
    return view('NewUser');
});

Route::get('about', function () {
    return view('about');
});

Route::get('contact', function () {
    return view('contact');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/admin', 'AdminController@index');
